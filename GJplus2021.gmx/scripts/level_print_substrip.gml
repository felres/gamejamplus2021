var substr = tmc_string_between(global.strip, strip_start_index, strip_end_index);

show_debug_message("PRINTING LEVEL NO."+string(number)+":");
show_debug_message("blocks_array length: " + string(array_length_1d(blocks_array)));
show_debug_message("substring length: " + string(string_length(substr)));
show_debug_message("substr[" + string(strip_start_index) + "," + string(strip_end_index) + "]: " + substr);
//for(var i = 0; i < array_length_1d(blocks_array); i++)
