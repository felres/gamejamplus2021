/// strip_save( filename )
var fname = argument0;
if file_text_open_write_all(fname, global.strip)
{
    show_debug_message("saved at direction: " + fname);
    clipboard_set_text(fname);
    show_debug_message("Copied to clipboard!")
    return 0;
}


return error("couldnt save new strip", 1);
