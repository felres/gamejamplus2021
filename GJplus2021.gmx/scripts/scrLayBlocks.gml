///scrLayBlocks( list containing rows );
var rows_list = argument0;
//if ds_exists(rows_list, ds_type_list){return error("list nonexistant", 1)};

for(var row = 0; row < ds_list_size(rows_list); row++)
{
    var str = ds_list_find_value(rows_list, row);
    for(var col = 1; col <= string_length(str); col++)
    {
        var char = string_char_at(str, col);
        create_block(char, row, col);
    }
}

return 0;
