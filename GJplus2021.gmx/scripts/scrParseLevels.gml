/***************************************************
  returns true if succesful
 ***************************************************/
/*var filename = argument0
var delim = ":";

/// open file
var file = file_text_open_read(filename);
if (file < 0) return false;

/// parse
// state enum
enum states {title, body, spec};
state = states.title;
// read file
while (!file_text_eof(file)) {
    var strline = file_text_read_string(file);
    
    switch(state)
    {
        case states.title:
            if(strline != "")
            {
                if( string_char_at(strline, 1) == delim )
                    add_char_to_block_dictionary(strline, delim);
                else
                {
                    // Nuevo nivel. Crear titulo y referencia en el map
                    ds_list_add(level_titles_list, strline);
                    var grid_rows = ds_list_create();
                    ds_map_add_list(level_grids_map, strline, grid_rows);
                    state = states.body;
                }
            }
            break;
        case states.body:
            if(strline == "")
                state = states.title;
            else
            {
                // add new level row
                var current_level_name = ds_list_find_value(level_titles_list, ds_list_size(level_titles_list)-1);
                var grid_rows = ds_map_find_value(level_grids_map, current_level_name);
                ds_list_add(grid_rows, strline);
            }
            break;
    }
    file_text_readln(file);
}
file_text_close(file);

return true;
