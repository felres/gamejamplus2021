var entry = global.entries_array[array_length_1d(global.entries_array)-1]
with entry 
{
    strip_end_index = argument0-1;
    entry_update_variables();
}
