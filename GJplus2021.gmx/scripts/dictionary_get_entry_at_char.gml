/// dictionary_get_entry_at_char(char)
var char = argument0;
for(var i=0; i<array_length_1d(global.entries_array); i++)
{
    var entry = global.entries_array[i];
    if entry.args_array[0] = char
        return entry;
}

return noone;
