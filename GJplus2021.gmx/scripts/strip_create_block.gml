// strip_create_block(i)
// Creates block, stores info in its respective level parent
var i = argument0;
var level = global.levels_array[array_length_1d(global.levels_array)-1];
var block = instance_create(x, y, objBlock);
with block
{
    strip_index = i;
    my_level_parent = level;
}
// Push new block instance into array
level.blocks_array[array_length_1d(level.blocks_array)] = block;
