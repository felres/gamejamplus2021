/// strip_update( filename )
/***************************************************
  Updates global.strip to the one in the file
 ***************************************************/

var fname = argument0;
var temp = file_text_open_read_all(fname);
if temp != undefined
    
{
    global.strip = temp;
    show_debug_message("Stored in strip file from: " + fname);
    clipboard_set_text(fname);
    show_debug_message("Copied to clipboard!")
    return 0;
}
else
    return error("File not found: " + fname, 1);
