/// add_char_to_block_dictionary( strline, delim )

var arglist = string_parse(argument0, argument1, false);
ds_list_delete(arglist, 0);
var ch = ds_list_find_value(arglist, 0);
ds_list_delete(arglist, 0);

ds_map_add_list(block_dict_map, ch, arglist);
