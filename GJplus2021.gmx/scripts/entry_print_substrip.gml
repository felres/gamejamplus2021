var substr = tmc_string_between(global.strip, strip_start_index, strip_end_index);

show_debug_message("PRINTING ENTRY");
show_debug_message("substring length: " + string(string_length(substr)));
show_debug_message("substr[" + string(strip_start_index) + "," + string(strip_end_index) + "]: " + substr);

entry_update_variables();
for(var i = 0; i < array_length_1d(args_array); i++)
    show_debug_message("arg"+string(i)+": "+ string(args_array[i]) );
