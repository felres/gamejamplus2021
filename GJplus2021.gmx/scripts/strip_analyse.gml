/// strip_analyse()
/***************************************************
  Reads through every char in the strip string.
  Creates entries and levels. Relegates responsibilities.
 ***************************************************/
enum estates {start, level, entry};
var state = estates.start;
var total_levels = 0;
var lastchar = "";
var i = 1;
while(i <= string_length(global.strip))
{
    var char = string_char_at(global.strip, i);
    
    switch(state)
    {
        case estates.start:
            switch(char)
            {
                case ":":
                    // New entry
                    var entry = instance_create(x, y, objDictionaryEntry);
                    with entry strip_start_index = i;
                    global.entries_array[array_length_1d(global.entries_array)] = entry;
                    state = estates.entry;
                    break;
                case "#":
                    // Nada, brincarse esta parte
                    break;
                default:
                    // New level
                    var lvl = instance_create(x, y, objLevel);
                    with lvl
                    {
                        strip_start_index = i;
                        number = total_levels;
                    }
                    total_levels++;
                    // Push new level instance into array
                    global.levels_array[array_length_1d(global.levels_array)] = lvl;
                    strip_create_block(i);
                    state = estates.level;
                    break;
            }
            break;
        case estates.level:
            switch(char)
            {
                case ":":
                    if(lastchar == "#")
                    {
                        strip_set_level_end(i);
                        state = estates.entry;
                    }
                    break;
                case "#":
                    if(lastchar == "#")
                    {
                        strip_set_level_end(i-1);
                        state = estates.start;
                        break;
                    }
                default:
                    strip_create_block(i);
                    break;
            }
            break;
        case estates.entry:
            switch(char)
            {
                case "#":
                    strip_set_entry_end(i);
                    state = estates.start;
                    break;
            }
            break;
        default:
            return error("unknown state", 1);
    }
    
    lastchar = char;
    i++;
}


/// End case: Close open level or entry readings
switch(state)
{
    case estates.start:
        // cerró bien
        break;
    case estates.level:
        strip_set_level_end(i);
        break;
    case estates.entry:
        strip_set_entry_end(i);
        break;
}

return 0;
