var char = string_char_at(global.strip, strip_index);

var entry_n = dictionary_get_number_at_char(char);
if entry_n != -1
{
    var new_n = (entry_n+1) mod array_length_1d(global.entries_array);
    var entry = dictionary_get_entry_at_number(new_n);
    sprite_index = asset_get_index(entry.args_array[1]);
    image_index = real(entry.args_array[2]);
    image_speed = real(entry.args_array[3]);
    visible = true;
    block_overwrite_strip(entry.args_array[0]);
    return 0;
}

return 1;
