/***************************************************
  Reviews block in global.blocks_strip array and
  updates them all
 ***************************************************/
for(var i = 0; i < array_length_1d(global.levels_array); i++)
{
    var level = global.levels_array[i];
    with level
        update_blocks();
}
