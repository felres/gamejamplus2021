/// drag_and_drop(button sprite?)

// check for globals
if !variable_global_exists("cursor_dnd")
{
    global.cursor_dnd = noone;
    global.cursor_xoffset = 0;
    global.cursor_yoffset = 0;
}

// variables
var xx = mouse_x;
var yy = mouse_y;
var isbutton = argument0;
if isbutton image_speed = 0;

// inputs
var hold = mouse_check_button(mb_left);
var pressed = mouse_check_button_pressed(mb_left);
var released = mouse_check_button_released(mb_left);

if position_meeting(xx, yy, id) && pressed && !instance_exists(global.cursor_dnd)
{
    global.cursor_dnd = id;
    global.cursor_xoffset = x-xx;
    global.cursor_yoffset = yy-y;
}


if global.cursor_dnd == id
{
    x = xx+global.cursor_xoffset;
    y = yy-global.cursor_yoffset;
    if isbutton image_index = 1;
    if released
    {
        global.cursor_dnd = noone;
        if isbutton image_index = 0;
    }
}
