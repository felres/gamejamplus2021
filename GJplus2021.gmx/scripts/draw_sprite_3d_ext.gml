///draw_sprite_3d_ext(sprite, pixel height, quality, x, y, xscale, yscale, extrusion scale, rotation, color, alpha)

var sprite = argument[0]
var layers = sprite_get_number(argument[0])
var height = argument[1]
var quality = abs(argument[2])
var _x = argument[3]
var _y = argument[4]
var _image_xscale = argument[5]
var _image_yscale = argument[6]
var _extrude_yscale = argument[7]
var rotation = argument[8]
var color = argument[9]
var alpha = argument[10]

for (i = 0; i < layers; i += 1){
    if (quality >= 1) {
        var q = 0;
        
        repeat(quality){
            q += height/quality
            draw_sprite_ext(sprite, i, _x, (_y - (i*height + q)*_extrude_yscale), _image_xscale, _image_yscale, rotation, color, alpha)
        }
    }else {
        draw_sprite_ext(sprite, i, _x, _y - (i*height)*_extrude_yscale, _image_xscale, _image_yscale, rotation, color, alpha)
    }
}
