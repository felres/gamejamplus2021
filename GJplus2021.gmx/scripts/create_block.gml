var char = argument0;
var row = argument1;
var col = argument2;

var args = ds_map_find_value(block_dict_map, char);
if is_undefined(args) return error("char '"+char+"' not in dictionary", 1);
ds_list_print(args);

var t;
var w = 16;
var h = 16;
var sprite = sprBlock;
var image = 0;
var imgspd = 1;
t = ds_list_find_value(args, 0);
if(t != "") w = real(t);
t = ds_list_find_value(args, 1);
if(t != "") h = real(t);
t = ds_list_find_value(args, 2);
if (asset_get_index(t) != -1) sprite = asset_get_index(t);
t = ds_list_find_value(args, 3);
if(t != "") image = real(t);
t = ds_list_find_value(args, 4);
if(t != "") imgspd = real(t);
with instance_create(x + (col*w), y + (row*h), objBlock)
{
    char_display = char;
    sprite_index = sprite;
    image_index = image;
    image_speed = imgspd;    
}

return 0;
