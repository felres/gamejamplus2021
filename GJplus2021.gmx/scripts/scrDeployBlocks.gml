global.strip = file_text_open_read_all(argument0);

var i = 1;
while(i <= string_length(global.strip))
{
    var ins = instance_create(x, y, objBlock);
    global.blocks_strip[i] = ins;
    with ins strip_index = i;
    
    i++;
}
