/// update_blocks()
for(var i = 0; i < array_length_1d(blocks_array); i++)
{
    var block = blocks_array[i];
    with block
        block_update();
}
