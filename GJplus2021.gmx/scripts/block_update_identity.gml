var char = string_char_at(global.strip, strip_index);

var entry = dictionary_get_entry_at_char(char);
if entry != noone
{
    sprite_index = asset_get_index(entry.args_array[1]);
    image_index = real(entry.args_array[2]);
    image_speed = real(entry.args_array[3]);
    visible = true;
    return 0;
}

return 1;
