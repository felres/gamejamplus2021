/// ds_list_print(list)
var list = argument0;
show_debug_message("LIST(no."+string(list)+"):");
for(var i = 0; i < ds_list_size(list); i++)
{
    show_debug_message(string_lpad(string(i),3,"0")+":"+string(ds_list_find_value(list,i)));
}
