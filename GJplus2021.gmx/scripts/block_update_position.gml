/// block_update_position()
// get info based on global.strip
var substr = tmc_string_between(global.strip,
                                my_level_parent.strip_start_index,
                                my_level_parent.strip_end_index);
var substr_index = strip_index - my_level_parent.strip_start_index;
// Only relevant part is the part of the grid before the block position
var substr_head = string_limit(substr, substr_index, "");

// We need to get the row where the char is. We count the amount 
// of "new lines" until the char is met
// if 0: first line
// if 1: second line
// if 2: third line
var row = string_count("#", substr_head);

// What column excactly?
// We get the place in string where we find the last new line
// but since the char we are on
var last_char_index = tmc_string_pos_last("#", substr_head);
// This difference, distance from last "new line"
var col = substr_index - last_char_index;

var xx = my_level_parent.x;
var yy = my_level_parent.y;
x = xx + (col*sprite_width);
y = yy + (row*sprite_height);

/*
show_debug_message("-BLOCK UPDATED-");
show_debug_message("substr_index: " + string(substr_index));
show_debug_message("substr_head: " + substr_head);
show_debug_message("row: " + string(row));
show_debug_message("col: " + string(col));
show_debug_message("x: " + string(x));
show_debug_message("y: " + string(y));
*/
