for(var i = 0; i < array_length_1d(global.entries_array); i++)
{
    var entry = global.entries_array[i];
    with entry
        entry_update_variables();
}
